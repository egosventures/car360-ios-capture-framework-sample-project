//
//  SpinDisplayViewController.swift
//  Car360FrameworksTest
//
//  Created by Remy Cilia on 1/31/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import Car360Capture

class SpinDisplayViewController: UIViewController {
    @IBOutlet weak var spinViewerView: SpinViewerView!
    var spinId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard
            let safeSpinId = spinId,
            let localDataSource = LocalSpinViewerDataSource(spinId: safeSpinId) else {
                dismiss(animated: true, completion: nil)
                return
        }
        
        spinViewerView.dataSource = localDataSource
    }
}
