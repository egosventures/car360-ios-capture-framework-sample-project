//
//  LoginViewController.swift
//  Car360FrameworksTest
//
//  Created by Remy Cilia on 2/22/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import MBProgressHUD

class LoginViewController: UIViewController {
    private struct Constants {
        private init() {}
        
        static let defaultUsername = ""
        static let defaultPassword = ""
        static let loginToMainSegueIdentifier = "loginToMainSegue"
    }
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usernameTextField.text = Constants.defaultUsername
        passwordTextField.text = Constants.defaultPassword
    }

    // MARK: - Login
    
    private func login(completion: @escaping (APIError?) -> ()) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        Car360CoreFramework.login(
            username: usernameTextField.text ?? "",
            password: passwordTextField.text ?? "") { (error: APIError?) -> (Void) in
                MBProgressHUD.hide(for: self.view, animated: true)
                completion(error)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func loginAction(_ sender: Any) {
        login { (error) in
            if let error = error {
                let alert = UIAlertController(title: "Login error", message: error.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: Constants.loginToMainSegueIdentifier, sender: self)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
