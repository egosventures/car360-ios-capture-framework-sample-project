//
//  CaptureSDKViewController.swift
//  Car360FrameworksTest
//
//  Created by Remy Cilia on 1/23/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import Car360Capture
import MBProgressHUD

class CaptureSDKViewController: UIViewController {
    private struct Constants {
        private init() {}
        
        static let workflowName = "Workflow name here"
    }
    
    @IBOutlet weak var newCaptureButton: UIBarButtonItem!
    @IBOutlet weak var capturesTableView: UITableView!
    var captures = [LocalCapture]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadWorkflows()
        self.loadCaptures()
    }
    
    // MARK: - Load captures
    
    private func loadCaptures() {
        captures = (try? CaptureManager.listCaptures()) ?? [LocalCapture]()
        capturesTableView.reloadData()
    }
    
    // MARK: - Load workflows
    
    private func loadWorkflows() {
        WorkflowManager.getAssignedWorkflows { [weak self] (assignedWorkflows: [LocalWorkflow]) -> (Void) in
            self?.newCaptureButton.isEnabled = assignedWorkflows.count > 0
        }
    }
    
    // MARK: - Build capture
    
    private func buildCapture() -> String? {
        guard let assignedWorkflows = LocalUser.currentUser?.workflowsArray,
            assignedWorkflows.count > 0 else {
                return nil
        }
        
        guard let workflow = assignedWorkflows.first(where: { $0.name == Constants.workflowName }),
            let workflowId = workflow.id else {
                return nil
        }
        
        let captureId: String
        
        do {
            captureId = try CaptureManager.buildCapture(withWorkflowId: workflowId, vinNumber: "TESTVIN")
        } catch {
            switch error {
            case Car360FrameworkError.userNotLoggedIn:
                print("User not logged in")
            case CaptureManager.BuildCaptureError.noIdentificationFound:
                print("no identification found")
            case CaptureManager.BuildCaptureError.workflowIdNotFound:
                print("no workflow found")
            default:
                break
            }
            
            return nil
        }
        
        return captureId
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let captureDetailsVC = segue.destination as? CaptureViewController {
            if let selectedIndexPath = capturesTableView.indexPathForSelectedRow {
                captureDetailsVC.captureId = captures[selectedIndexPath.row].id
            } else {
                captureDetailsVC.captureId = buildCapture()
            }
        }
    }
}

extension CaptureSDKViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "captureCell"
        var cell: UITableViewCell! = capturesTableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: reuseIdentifier)
        }
        
        cell.textLabel?.text = captures[indexPath.row].id
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? captures.count : 0
    }
}

extension CaptureSDKViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "captureSDKVCToCaptureVC", sender: nil)
        capturesTableView.deselectRow(at: indexPath, animated: true)
    }
}
