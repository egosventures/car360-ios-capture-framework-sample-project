//
//  CaptureDetailsViewController.swift
//  Car360FrameworksTest
//
//  Created by Remy Cilia on 1/23/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import Car360Capture
import MBProgressHUD

class CaptureViewController: UIViewController {
    // MARK: - Properties
    
    // MARK: - IBOutlets
    @IBOutlet weak var captureIdLabel: UILabel!
    @IBOutlet weak var spinsLabel: UILabel!
    @IBOutlet weak var interiorsLabel: UILabel!
    @IBOutlet weak var stillsLabel: UILabel!
    @IBOutlet weak var captureSpinButton: UIBarButtonItem!
    @IBOutlet weak var captureInteriorButton: UIBarButtonItem!
    @IBOutlet weak var captureStillButton: UIBarButtonItem!
    @IBOutlet weak var openSpinViewerButton: UIButton!
    @IBOutlet weak var openInteriorViewerButton: UIButton!
    @IBOutlet weak var uploadCaptureButton: UIBarButtonItem!
    @IBOutlet weak var deleteCaptureButton: UIBarButtonItem!
    
    // MARK: Private properties
    var captureId: String!
    var capture: LocalCapture!
    var progressHUD: MBProgressHUD?
    
    // MARK: - UIViewController methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCapture()
        udpateLabels()
        updateCaptureButtons()
    }
    
    // MARK: - Loading methods
    
    private func loadCapture() {
        guard let captureId = captureId,
            let capture = try? CaptureManager.getCapture(withId: captureId) else {
                fatalError("Trying to open CaptureDetailsViewController without capture id")
        }
        
        self.capture = capture
    }
    
    // MARK: - UI
    
    // MARK: Labels update
    private func udpateLabels() {
        captureIdLabel.text = capture.id
        
        let spinsCount = getSpinsCount()
        spinsLabel.text = "\(spinsCount.total) spins (\(spinsCount.empty) empty)"
        
        let interiorsCount = getInteriorsCount()
        interiorsLabel.text = "\(interiorsCount.total) interiors (\(interiorsCount.empty) empty)"
        
        let stillsCount = getStillsCount()
        stillsLabel.text = "\(stillsCount.total) stills (\(stillsCount.empty) empty)"
    }
    
    private func getSpinsCount() -> (total: Int, empty: Int) {
        let spins = capture.spinsArray
        let totalSpins = spins.count
        let emptySpins = spins.filter({ $0.isEmpty }).count
        
        return (totalSpins, emptySpins)
    }

    private func getInteriorsCount() -> (total: Int, empty: Int) {
        let interiors = capture.interiorImagesArray
        let totalInteriors = interiors.count
        let emptyInteriors = interiors.filter({ $0.isEmpty }).count
        
        return (totalInteriors, emptyInteriors)
    }
    
    private func getStillsCount() -> (total: Int, empty: Int) {
        let stills = capture.stillImagesArray
        let totalStills = stills.count
        let emptyStills = stills.filter({ $0.isEmpty }).count
        
        return (totalStills, emptyStills)
    }
    
    // MARK: Buttons update
    private func updateCaptureButtons() {
        updateSpinsButton()
        updateInteriorButton()
        updateStillsButton()
        updateOpenSpinViewerButton()
        updateOpenInteriorViewerButton()
        updateUploadCaptureButton()
    }
    
    private func updateSpinsButton() {
        let emptySpinsCount = capture.spinsArray.filter({ $0.isEmpty }).count
        captureSpinButton.isEnabled = emptySpinsCount > 0
    }
    
    private func updateInteriorButton() {
        let emptyInteriorsCount = capture.interiorImagesArray.filter({ $0.isEmpty }).count
        captureInteriorButton.isEnabled = emptyInteriorsCount > 0
    }
    
    private func updateStillsButton() {
        let emptyStillsCount = capture.stillImagesArray.filter({ $0.isEmpty }).count 
        captureStillButton.isEnabled = emptyStillsCount > 0
    }
    
    private func updateOpenSpinViewerButton() {
        let nonEmptySpinsCount = capture.spinsArray.filter({ !$0.isEmpty }).count 
        openSpinViewerButton.isEnabled = nonEmptySpinsCount > 0
    }
    
    private func updateOpenInteriorViewerButton() {
        let nonEmptyInteriorsCount = capture.interiorImagesArray.filter({ !$0.isEmpty }).count 
        openInteriorViewerButton.isEnabled = nonEmptyInteriorsCount > 0
    }
    
    private func updateUploadCaptureButton() {
        let capturedMandatoryCount = capture.capturedMandatoryCount
        uploadCaptureButton.isEnabled = capturedMandatoryCount.done >= capturedMandatoryCount.total
    }
    
    // MARK: - Capture spin
    
    private func captureSpin() {
        if let spinCaptureVC = try? Car360Capture.getSpinCaptureViewController() {
            let config = SpinCaptureConfiguration()
            config.id = capture.spinsArray.first(where: { $0.isEmpty })?.id
            
            if config.id == nil {
                return
            }
            
            config.captureType = .walkAround
            
            spinCaptureVC.spinCaptureConfiguration = config
            spinCaptureVC.delegate = self
            spinCaptureVC.view.backgroundColor = .black
            
            present(spinCaptureVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - Capture Interior
    
    private func captureInterior() {
        guard let interiorImageId = capture.interiorImagesArray.first(where: { $0.isEmpty })?.id else {
            return
        }
        
        guard let tempPath = getTempInteriorImagePath() else {
            return
        }
        
        try? InteriorImageManager.saveInteriorImage(withTempImagePath: tempPath, inInteriorImageId: interiorImageId)
        
        // reload UI
        udpateLabels()
        updateCaptureButtons()
    }
    
    private func getTempInteriorImagePath() -> String? {
        guard let imagePath = Bundle.main.path(forResource: "interior-test", ofType: "jpg") else {
            return nil
        }
        
        let tempPath = "\(NSTemporaryDirectory())/temp-interior-test.jpg"
        
        do {
            try FileManager.default.copyItem(atPath: imagePath, toPath: tempPath)
        } catch {
            return nil
        }
        
        return tempPath
    }
    
    // MARK: - Capture still
    
    private func captureStill() {
        if capture.stillImagesArray.first(where: { $0.isEmpty }) == nil {
            return
        }
        
        if let stillCaptureVC = try? Car360Capture.getStillCaptureViewController() {
            stillCaptureVC.delegate = self
            stillCaptureVC.view.backgroundColor = .black
            
            present(stillCaptureVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - Upload
    
    private func registerUploadNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(uploadAddedToQueue(sender:)),
            name: CaptureUploader.NotificationName.addedToQueue.name,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(uploadStarted(sender:)),
            name: CaptureUploader.NotificationName.exportStarted.name,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(uploadProgressed(sender:)),
            name: CaptureUploader.NotificationName.exportProgressed.name,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(uploadCompleted(sender:)),
            name: CaptureUploader.NotificationName.exportCompleted.name,
            object: nil)
    }
    
    private func unregisterUploadNotifications() {
        NotificationCenter.default.removeObserver(self, name: CaptureUploader.NotificationName.addedToQueue.name, object: nil)
        NotificationCenter.default.removeObserver(self, name: CaptureUploader.NotificationName.exportStarted.name, object: nil)
        NotificationCenter.default.removeObserver(self, name: CaptureUploader.NotificationName.exportProgressed.name, object: nil)
        NotificationCenter.default.removeObserver(self, name: CaptureUploader.NotificationName.exportCompleted.name, object: nil)
    }

    @objc func uploadAddedToQueue(sender: Notification) {
        progressHUD?.detailsLabel.text = "Added to queue..."
        progressHUD?.progress = 0
    }
    
    @objc func uploadStarted(sender: Notification) {
        progressHUD?.detailsLabel.text = "Packaging..."
        progressHUD?.progress = 0
    }
    
    @objc func uploadProgressed(sender: Notification) {
        guard
            let userInfo = sender.userInfo,
            let status = userInfo[CaptureUploader.NotificationField.status] as? CaptureExportOperation.ExportStatus else {
                return
        }
        
        if case let CaptureExportOperation.ExportStatus.uploading(progress) = status {
            let progressValue = Float(progress.fractionCompleted)
            let progressPercent = progressValue * 100
            progressHUD?.detailsLabel.text = "Uploading (\(String(format: "%.1f", progressPercent))%)..."
            progressHUD?.progress = progressValue
        }
    }
    
    @objc func uploadCompleted(sender: Notification) {
        guard
            let userInfo = sender.userInfo,
            let result = userInfo[CaptureUploader.NotificationField.result] as? CaptureExportOperation.ExportResult else {
                return
        }
        
        switch result {
        case .success( _):
            print("upload succeded")
        case .error(let exportError):
            print("upload failed: \(exportError.description)")
        }
        
        progressHUD?.progress = 1.0
        progressHUD?.hide(animated: true)
        unregisterUploadNotifications()
    }
    
    func uploadCapture() {
        guard let captureId = capture.id else {
            return
        }
        
        registerUploadNotifications()
        progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        progressHUD?.mode = .determinate
        progressHUD?.label.text = "Exporting capture"
        
        try? CaptureManager.uploadCapture(withId: captureId)
    }
    
    func deleteCapture() {
        guard let captureId = capture.id else {
            return
        }
        
        let deleteAlert = UIAlertController(title: "Delete", message: "Are you sure?", preferredStyle: .alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { [weak self] (alert) in
            try? CaptureManager.deleteCapture(withId: captureId)
            self?.navigationController?.popViewController(animated: true)
        }))
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        present(deleteAlert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func captureSpinAction(_ sender: Any) {
        captureSpin()
    }
    
    @IBAction func captureInteriorAction(_ sender: Any) {
        captureInterior()
    }
    
    @IBAction func captureStillAction(_ sender: Any) {
        captureStill()
    }
    
    @IBAction func uploadCaptureAction(_ sender: Any) {
        uploadCapture()
    }
    
    @IBAction func deleteCaptureAction(_ sender: Any) {
        deleteCapture()
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let spinDisplayVC = segue.destination as? SpinDisplayViewController {
            if let spinId = capture.spinsArray.first(where: { !$0.isEmpty })?.id {
                spinDisplayVC.spinId = spinId
            }
        } else if let interiorDisplayVC = segue.destination as? InteriorDisplayViewController {
            if let interiorId = capture.interiorImagesArray.first(where: { !$0.isEmpty })?.id {
                interiorDisplayVC.interiorImageId = interiorId
            }
        }
    }
    
    @IBAction func unwindToCaptureViewController(segue: UIStoryboardSegue) { }
}

// MARK: - Extensions

extension CaptureViewController: SpinCaptureDelegate {
    func spinCaptureViewControllerDidCancel(spinCaptureViewController: SpinCaptureViewController) {
        spinCaptureViewController.dismiss(animated: true, completion: nil)
    }
    
    func spinCaptureViewController(spinCaptureViewController: SpinCaptureViewController, didFinishCapturingSpin spinData: SpinData?) {
        if let spinData = spinData {
            try? SpinManager.saveSpin(withSpinData: spinData)
        }
        
        spinCaptureViewController.dismiss(animated: true, completion: nil)
    }
}

extension CaptureViewController: StillCaptureDelegate {
    func stillCaptureViewControllerDidCancel(stillCaptureViewController: StillCaptureViewController) {
        stillCaptureViewController.dismiss(animated: true, completion: nil)
    }
    
    func stillCaptureViewController(stillCaptureViewController: StillCaptureViewController, didCaptureMediaAtPath path: String,
                                    mediaType type: MediaType) {
        
        if let stillImageId = capture.stillImagesArray.first(where: { $0.isEmpty == true })?.id {
            try? StillImageManager.saveStillImage(withTempImagePath: path, inStillImageId: stillImageId)
        }
        
        stillCaptureViewController.dismiss(animated: true, completion: nil)
    }
}
