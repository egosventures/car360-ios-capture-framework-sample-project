//
//  InteriorDisplayViewController.swift
//  Car360FrameworksTest
//
//  Created by Remy Cilia on 1/31/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import Car360Capture

class InteriorDisplayViewController: UIViewController {
    @IBOutlet weak var interiorViewerView: InteriorViewerView!
    
    var interiorImageId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let safeInteriorImageId = interiorImageId {
            interiorViewerView.interiorImageId = safeInteriorImageId
        }
    }
}
